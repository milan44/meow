## Meow

Exactly like brainfuck but for cats.

```
+ = meOw
- = mEow
< = Meow
> = meoW
, = MeoW
. = mEOw
[ = meow
] = MEOW
```

[Example usage](https://asciinema.org/a/vE3IB7mwf3IDlCZggt1iwaUNb)

#### Hello world

```
meOw meow mEow meow Meow Meow meow meOw meow mEow mEow mEow meoW MEOW
mEow meow Meow Meow Meow MEOW MEOW MEOW meoW meoW meoW mEow MEOW meoW
mEow mEOw mEow mEow mEow mEOw meoW mEOw mEOw meoW mEOw Meow Meow Meow
Meow mEow mEOw Meow meOw mEOw meoW meoW meoW meoW meoW mEOw meoW mEOw
Meow Meow mEOw Meow mEow mEOw
```