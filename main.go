package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
)

var Debug = false

func main() {
	flag.BoolVar(&Debug, "debug", false, "Debug the code")
	flag.Parse()

	if Debug {
		fmt.Println("[Debug mode enabled]")
	}
	fmt.Println("Input meow source and add trailing \"end\"")
	fmt.Print("> ")
	sourceCode := readline()
	for !strings.HasSuffix(sourceCode, "end") {
		sourceCode += readline()
	}
	fmt.Println("Tokenizing...")
	tokens := tokenize(sourceCode)

	fmt.Println("Running...")
	run(tokens)
	debug("exit")
}

func debug(s string) {
	if Debug {
		fmt.Print("[" + s + "]")
	}
}

func readline() string {
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')

	text = strings.Replace(text, "\r\n", "", -1)
	text = strings.Replace(text, "\n", "", -1)

	return text
}
