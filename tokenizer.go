package main

import (
	"fmt"
	"regexp"
	"strconv"
)

type Token struct {
	tokenType string
	value     []Token
	index     int
}

const TypePlus = "add"
const TypeMinus = "sub"
const TypeLeft = "left"
const TypeRight = "right"
const TypeInput = "input"
const TypeOutput = "output"
const TypeLoop = "loop"

func tokenize(source string) []Token {
	rgx := regexp.MustCompile(`(?mi)meow`)
	words := rgx.FindAllString(source, -1)

	tokens, _ := extractTokens(words, 0)
	return tokens
}

func extractTokens(words []string, index int) ([]Token, int) {
	var tokens []Token

	for i, word := range words {
		if i < index {
			continue
		}
		index = i

		switch word {
		case "meOw":
			tokens = append(tokens, Token{
				tokenType: TypePlus,
				index:     i + 1,
			})
		case "mEow":
			tokens = append(tokens, Token{
				tokenType: TypeMinus,
				index:     i + 1,
			})
		case "Meow":
			tokens = append(tokens, Token{
				tokenType: TypeLeft,
				index:     i + 1,
			})
		case "meoW":
			tokens = append(tokens, Token{
				tokenType: TypeRight,
				index:     i + 1,
			})
		case "MeoW":
			tokens = append(tokens, Token{
				tokenType: TypeInput,
				index:     i + 1,
			})
		case "mEOw":
			tokens = append(tokens, Token{
				tokenType: TypeOutput,
				index:     i + 1,
			})
		case "meow":
			t, newIndex := extractTokens(words, i+1)
			tokens = append(tokens, Token{
				tokenType: TypeLoop,
				index:     i + 1,
				value:     t,
			})
			index = newIndex
		case "MEOW":
			return tokens, i + 1
		}
	}

	return tokens, index
}

func debugTokens(tokens []Token, ident string) {
	for _, token := range tokens {
		fmt.Println("[" + strconv.Itoa(token.index) + "] " + ident + token.tokenType)

		if token.tokenType == TypeLoop {
			debugTokens(token.value, ident+"  ")
		}
	}
}
