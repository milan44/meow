package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	fmt.Println("Input brainfuck source and add trailing \"end\"")
	fmt.Print("> ")
	sourceCode := readline()
	for !strings.HasSuffix(strings.TrimSpace(sourceCode), "end") {
		sourceCode += readline()
	}
	sourceCode = strings.TrimSpace(sourceCode)
	sourceCode = strings.TrimSpace(sourceCode[:len(sourceCode)-3])

	fmt.Println("Transpiling...")

	fmt.Println("---")
	fmt.Println(transpile(sourceCode))
	fmt.Println("---")
}

func readline() string {
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')

	return text
}
