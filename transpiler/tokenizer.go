package main

import (
	"strings"
)

const TypePlus = "+"
const TypeMinus = "-"
const TypeLeft = "<"
const TypeRight = ">"
const TypeInput = ","
const TypeOutput = "."
const TypeLoopStart = "["
const TypeLoopEnd = "]"

func transpile(source string) string {
	source = strings.ReplaceAll(source, TypePlus, "meOw ")
	source = strings.ReplaceAll(source, TypeMinus, "mEow ")
	source = strings.ReplaceAll(source, TypeLeft, "Meow ")
	source = strings.ReplaceAll(source, TypeRight, "meoW ")
	source = strings.ReplaceAll(source, TypeInput, "MeoW ")
	source = strings.ReplaceAll(source, TypeOutput, "mEOw ")
	source = strings.ReplaceAll(source, TypeLoopStart, "meow ")
	source = strings.ReplaceAll(source, TypeLoopEnd, "MEOW ")

	return strings.TrimSpace(source)
}