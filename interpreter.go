package main

import (
	"fmt"
	"github.com/eiannone/keyboard"
	"os"
	"strconv"
)

var memory = []int{0}
var pointer = 0

func run(tokens []Token) {
	for _, token := range tokens {
		switch token.tokenType {
		case TypePlus:
			debug("+")
			increment()
		case TypeMinus:
			debug("-")
			decrement()
		case TypeLeft:
			debug("<")
			pointer--
		case TypeRight:
			debug(">")
			pointer++
		case TypeInput:
			debug(",")
			input()
		case TypeOutput:
			debug(".")
			output()
		case TypeLoop:
			fillMemory()
			loops := 0
			if memory[pointer] <= 0 {
				debug("skip-loop")
			}
			for memory[pointer] > 0 {
				debug("loop-start")

				run(token.value)
				loops++
				fillMemory()

				if loops > 500 {
					fmt.Println("ERROR: Infinite loop at position '" + strconv.Itoa(token.index) + "'. (500+ runs)")
					os.Exit(1)
				}
				debug("loop-end")
			}
		}
	}
}

func fillMemory() {
	if pointer < 0 {
		panic("Memory error, pointer is below 0")
	}

	for pointer+1 >= len(memory) {
		memory = append(memory, 0)
	}
}

func input() {
	fillMemory()

	char, _, err := keyboard.GetSingleKey()
	if err != nil {
		fmt.Println("An error occurred while reading user input: " + err.Error())
		os.Exit(1)
	}

	if char == 0 {
		fmt.Println()
	} else {
		fmt.Print(string(char))
	}

	ascii := int(char)

	if ascii > 255 {
		ascii = 0
	}

	memory[pointer] = ascii
}

func output() {
	fillMemory()

	ascii := memory[pointer]
	fmt.Print(string(rune(ascii)))
}

func increment() {
	fillMemory()

	memory[pointer]++

	if memory[pointer] > 255 {
		memory[pointer] = 0
	}
}
func decrement() {
	fillMemory()

	memory[pointer]--

	if memory[pointer] < 0 {
		memory[pointer] = 255
	}
}
